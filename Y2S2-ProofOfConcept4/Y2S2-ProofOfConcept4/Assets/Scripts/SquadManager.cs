﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class SquadManager : MonoBehaviour
{
    private UnityAction<Squad> squadListener;

    public List<Squad> squads;

    public int resources;
    public Text resourceText;

    public int energy;
    public int maxEnergy = 10;
    public Slider energySlider;
    public float energyTimer;
    public float energyTimeLeft;

    public int drones = 1;
    public int droneSend = 1;
    public Text droneSendText;
    public Text droneCountText;
    public Text nextMissionCompleteText;

    public int factoryLevel;
    public float factoryCostMultiplier;
    public int factoryInitialCost;
    public int factoryCost;
    public Text factoryCostText;
    public Text factoryLevelText;

    public int droneCost = 0;
    public Text droneCostText;
    public int dronesToBuild;
    public Text dronesToBuildText;

    private void Awake()
    {
        squads = new List<Squad>();
        squadListener = new UnityAction<Squad>(ReturnEvent);
    }

    private void Update()
    {
        foreach (Squad i in squads)
        {
            i.TimePassed(Time.deltaTime);
        }

        if (Input.mouseScrollDelta.y != 0)
        {
            droneSend = Mathf.Clamp(droneSend + (int)Input.mouseScrollDelta.y, 1, 10000);
        }
        droneSendText.text = "Drones to Send: " + droneSend;

        if (Input.GetKeyDown(KeyCode.L))
        {
            foreach (Squad i in squads)
            {
                i.SkipTime(1795);
            }
        }

        if (energy < maxEnergy)
        {
            energyTimeLeft -= Time.deltaTime;
            if (energyTimeLeft <= 0)
            {
                energy++;
                energyTimeLeft = energyTimer;
            }
        }

        resourceText.text = "" + resources;
        energySlider.value = (float)energy / (float)maxEnergy;
        droneCountText.text = "Drones Available: " + drones;
        nextMissionCompleteText.text = GetNextFinishedMission();
        factoryLevelText.text = "Level " + factoryLevel;
        factoryCostText.text = "Cost: " + factoryCost;
        droneCostText.text = "Cost: " + (droneCost*dronesToBuild);
        dronesToBuildText.text = "Build " + dronesToBuild + " Drones";
    }
    public void SetDronesToBuild(Slider value)
    {
        dronesToBuild = (int)value.value;
    }
    public void UpgradeFacroty()
    {
        if(resources >= factoryCost)
        {
            factoryLevel++;
            droneCost--;
            resources -= factoryCost;
            factoryCost = Mathf.RoundToInt(Mathf.Pow(factoryInitialCost + factoryCostMultiplier, (float)factoryLevel * 0.2f));
        }
    }
    public void BuildDrone()
    {
        if(factoryLevel > 0)
        {
            if(resources >= (droneCost*dronesToBuild))
            {
                drones+= dronesToBuild;
                resources -= droneCost * dronesToBuild;
            }
        }
    }
    public string GetNextFinishedMission()
    {
        if (squads.Count > 0)
        {
            Squad nearest = squads[0];

            foreach (Squad i in squads)
            {
                if (i.missionTimeLeft < nearest.missionTimeLeft)
                {
                    nearest = i;
                }
            }
            return "Next mission return in: " + nearest.missionDurationText;
        }
        return "No active mission";
    }
    public void LaunchMission()
    {
        Debug.Log("Attempting to send a squad...");
        if (drones > 0)
        {
            if (energy > 0)
            {
                int dronesToSend = droneSend;
                if(dronesToSend > drones)
                {
                    dronesToSend = drones;
                }
                Squad newSquad = new Squad(1800, "test", dronesToSend);
                newSquad.SendTeam();
                newSquad.ListenForReturn(squadListener);

                energy--;
                drones -= dronesToSend;

                squads.Add(newSquad);
                Debug.Log("Sent out a new squad " + newSquad.name);
            }
            else
            {
                Debug.LogError("Not enough energy");
            }
        }
        else
        {
            Debug.LogError("Not enough drones");
        }
    }
    private void ReturnEvent(Squad squad)
    {
        resources += squad.missionValue * squad.drones;
        drones += squad.drones;

        squads.Remove(squad);
    }
}
