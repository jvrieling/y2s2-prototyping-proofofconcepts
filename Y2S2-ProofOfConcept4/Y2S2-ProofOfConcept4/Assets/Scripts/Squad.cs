﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

[System.Serializable]
public class SquadReturnEvent : UnityEvent<Squad> { }

[System.Serializable]
public class Squad
{
    public SquadReturnEvent returnEvent;

    public string name = "Temp";
    public int drones = 0;

    public float missionTimeLeft = 1800;
    public int missionValue = 30;
    public string squadStatus = "ready";

    public string missionDurationText;

    public Squad(float missionTime, string newName, int newDrones)
    {
        name = newName;
        missionTimeLeft = missionTime;
        returnEvent = new SquadReturnEvent();
        drones = newDrones;
    }

    public void TimePassed(float time)
    {
        if (squadStatus == "away")
        {
            missionTimeLeft -= time;
            if (missionTimeLeft <= 0)
            {
                returnHome();
            }
        }

        missionDurationText = justinStuff.FormatTime.FormatMS(missionTimeLeft);
    }

    public void ListenForReturn(UnityAction<Squad> listener)
    {
        returnEvent.AddListener(listener);
    }
    public void returnHome()
    {
        returnEvent.Invoke(this);
        squadStatus = "ready";
    }
    public void SendTeam()
    {
        if (squadStatus == "ready")
        {
            squadStatus = "away";

        }
    }
    public void SkipTime(float amt)
    {
        missionTimeLeft -= amt;
    }
}
