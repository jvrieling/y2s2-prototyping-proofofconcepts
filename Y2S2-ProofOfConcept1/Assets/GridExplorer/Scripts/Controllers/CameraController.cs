﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour
{
    private void Start()
    {
        Vector3 startPos = GridManager.instance.GetGridCenterWorldCoordinate();

        startPos.z = -10;

        transform.position = startPos;
    }
}
