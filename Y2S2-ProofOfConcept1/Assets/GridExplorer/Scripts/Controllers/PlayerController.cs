﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerController : MonoBehaviour
{
    // The x and y coordinates of the object on the grid, NOT of the object in world space.
    public int gridX = 0;
    public int gridY = 0;

    public int turns = 25;
    public int turnsLeft;

    public Text turnsLeftText;

    private void Start()
    {
        turnsLeft = turns;
        GridManager.instance.ExploreTile(new Vector2Int(gridX, gridY));
    }
    // Update is called once per frame
    void Update()
    {
        if (turnsLeft > 0)
        {
            if (Input.GetKeyDown(KeyCode.UpArrow) || Input.GetKeyDown(KeyCode.W))
            {
                Move(0, 1);
            }
            if (Input.GetKeyDown(KeyCode.DownArrow) || Input.GetKeyDown(KeyCode.S))
            {
                Move(0, -1);
            }
            if (Input.GetKeyDown(KeyCode.LeftArrow) || Input.GetKeyDown(KeyCode.A))
            {
                Move(-1, 0);
            }
            if (Input.GetKeyDown(KeyCode.RightArrow) || Input.GetKeyDown(KeyCode.D))
            {
                Move(1, 0);
            }
            turnsLeftText.text = "" + turnsLeft;
        }

        Vector2 newPosition = GridManager.instance.GetWorldCoordinatesFromGrid(gridX, gridY);
        newPosition.x += GridManager.instance.tileSize / 2;
        newPosition.y += GridManager.instance.tileSize / 2;
        transform.position = newPosition;

        if (turnsLeft <= 0)
        {
            GetComponentInChildren<Renderer>().material.color = Color.red;
        }
    }

    public void Move(int x, int y)
    {
        Move(new Vector2Int(x, y));
    }
    public void Move(Vector2Int distance)
    {
        if (GridManager.instance.IsInsideBounds(distance + new Vector2Int(gridX, gridY)))
        {
            gridX += distance.x;
            gridY += distance.y;
            GridManager.instance.ExploreTile(new Vector2Int(gridX, gridY));
            turnsLeft--;
        }
    }
    public void SetScale(Vector3 scale)
    {
        transform.GetChild(0).localScale = scale;
    }
    public void SetCoords(Vector2 newCoords)
    {
        gridX = Mathf.FloorToInt(newCoords.x);
        gridY = Mathf.FloorToInt(newCoords.y);
    }

}
