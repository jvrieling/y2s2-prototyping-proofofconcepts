﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GridObject : MonoBehaviour
{
    // The x and y coordinates of the object on the grid, NOT of the object in world space.
    public int gridX;
    public int gridY;

    public void CallRevealEvent()
    {
        if(gameObject.name == "Pyramid")
        {
            GridManager.instance.foundPyramids++;
            GridManager.instance.pyramidsFoundText.text = GridManager.instance.foundPyramids + "/" + GridManager.instance.pyramidCount;
        }
    }
    public void CallCollectEvent()
    {
        if(gameObject.name == "Turn Pickup")
        {
            GridManager.instance.player.turnsLeft += 5;
            Destroy(gameObject);
        }
    }

    public void SetCoords(Vector2 newCoords)
    {
        gridX = Mathf.FloorToInt(newCoords.x);
        gridY = Mathf.FloorToInt(newCoords.y);
    }
}
