﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public enum TileRevealState { invisible, fog, reveal };
public class GridTile : MonoBehaviour
{
    // The x and y coordinates of the object on the grid, NOT of the object in world space.
    public int gridX;
    public int gridY;

    public Color fogColor;

    public GridObject containedObject;

    public TileRevealState revealState;
    private void Start()
    {
        SetRevealState(TileRevealState.invisible);
    }
    private void Update()
    {
        transform.position = GridManager.instance.GetWorldCoordinatesFromGrid(gridX, gridY);
        UpdateRevealState();
    }
    public void ObjectRevealEvent()
    {
        if(containedObject != null)
        {
            containedObject.CallRevealEvent();
        }
    }
    public void ObjectCollectEvent()
    {
        if (containedObject != null)
        {
            containedObject.CallCollectEvent();
        }
    }
    public void SetRevealState(TileRevealState state)
    {
        if (revealState != state)
        {
            revealState = state;

            if (state == TileRevealState.invisible)
            {
                SetActiveChildren(false);
            }
            else
            {
                SetActiveChildren(true);
            }

            UpdateRevealState();
        }
    }
    public void UpdateRevealState()
    {
        switch (revealState)
        {
            case TileRevealState.fog:
                GetComponentInChildren<Renderer>().material.color = fogColor;
                break;
            case TileRevealState.reveal:
                GetComponentInChildren<Renderer>().material.color = Color.white;
                break;
        }
    }
    public void SetActiveChildren(bool a)
    {
        foreach (Transform child in transform)
        {
            child.gameObject.SetActive(a);
        }
    }
    public void SetContainedObject(GridObject newObj)
    {
        containedObject = newObj;
        containedObject.gameObject.transform.parent = transform;
        containedObject.transform.position = new Vector3(GridManager.instance.tileSize/2, GridManager.instance.tileSize / 2, -0.35f);
        containedObject.transform.localScale = transform.localScale;
    }
    public void SetCoordinates(int x, int y)
    {
        gridX = x;
        gridY = y;
    }
}
