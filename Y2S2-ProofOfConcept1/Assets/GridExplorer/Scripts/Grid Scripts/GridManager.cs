﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class GridManager : MonoBehaviour
{
    public static GridManager instance;

    public int gridWidth;
    public int gridHeight;
    public float tileSize = 1f;
    public float tileGap = 0f;

    public GameObject tilePrefab;

    public GridTile[,] grid;

    public GameObject pyramidPrefab;
    public int pyramidCount = 4;
    public GridObject[] pyramids;
    [HideInInspector]
    public int foundPyramids;
    public Text pyramidsFoundText;

    public int pickupCount;
    public GameObject pickupPrefab;

    public PlayerController player;

    private void Awake()
    {
        if (GridManager.instance == null)
        {
            instance = this;
        }

        grid = new GridTile[gridWidth, gridHeight];
        pyramids = new GridObject[pyramidCount];

        GenerateGrid();
        GeneratePyramids();
        GenerateTurnPickups();

        player.SetScale(new Vector3(tileSize, tileSize, tileSize));
    }
    private void Update()
    {
        if (GridManager.instance == null)
        {
            instance = this;
        }
    }
    public void ExploreTile(Vector2Int coordinates)
    {
        ExploreTileSingle(new Vector2Int(coordinates.x, coordinates.y), TileRevealState.reveal, true);
        ExploreTileSingle(new Vector2Int(coordinates.x + 1, coordinates.y), TileRevealState.reveal);
        ExploreTileSingle(new Vector2Int(coordinates.x - 1, coordinates.y), TileRevealState.reveal);
        ExploreTileSingle(new Vector2Int(coordinates.x, coordinates.y + 1), TileRevealState.reveal);
        ExploreTileSingle(new Vector2Int(coordinates.x, coordinates.y - 1), TileRevealState.reveal);
    }
    private void ExploreTileSingle(Vector2Int coords, TileRevealState state, bool callCollectEvent = false)
    {
        if (IsInsideBounds(coords))
        {
            GridTile tile = grid[coords.x, coords.y];
            // If the tile has not been seen yet, call the reveal event.
            if (tile.revealState == TileRevealState.invisible)
                tile.ObjectRevealEvent();
            // True should be passed if the player is on the tile that is being revealed.
            if (callCollectEvent)
                tile.ObjectCollectEvent();
            tile.SetRevealState(state);
        }
    }
    public bool IsInsideBounds(Vector2Int coords)
    {
        return coords.x < gridWidth && coords.x >= 0 && coords.y < gridHeight && coords.y >= 0;
    }

    public void RegisterTile(GridTile newObj)
    {
        grid[newObj.gridX, newObj.gridY] = newObj;
    }

    private void GenerateGrid()
    {
        for (int i = 0; i < gridWidth; i++)
        {
            for (int j = 0; j < gridHeight; j++)
            {
                CreateGridTile(i, j);
            }
        }
    }

    public void GeneratePyramids()
    {
        List<Vector2> pyramidCoords = new List<Vector2>();

        // Create each pyramid.
        for (int i = 0; i < pyramidCount; i++)
        {
            Vector2 temp;
            // Ensure there are no duplicate coordinates.
            do
            {
                temp = new Vector2(Random.Range(0, gridWidth), Random.Range(0, gridHeight));
            } while (pyramidCoords.Contains(temp));
            pyramidCoords.Add(temp);
        }

        // for each of the previously made coordinates, create a pyramid and attach it to that tile.
        for (int i = 0; i < pyramidCount; i++)
        {
            GridObject tempPyramid = Instantiate(pyramidPrefab, transform.position, Quaternion.identity).GetComponent<GridObject>();
            tempPyramid.SetCoords(pyramidCoords[i]);
            tempPyramid.gameObject.name = "Pyramid";

            grid[Mathf.FloorToInt(pyramidCoords[i].x), Mathf.FloorToInt(pyramidCoords[i].y)].SetContainedObject(tempPyramid);

            pyramids[i] = tempPyramid;
        }
    }
    public void GenerateTurnPickups()
    {
        List<Vector2Int> pickupCoords = new List<Vector2Int>();

        // Create each pyramid.
        for (int i = 0; i < pickupCount; i++)
        {
            Vector2Int temp;
            // Ensure there are no duplicate coordinates.
            do
            {
                temp = new Vector2Int(Random.Range(0, gridWidth), Random.Range(0, gridHeight));
            } while (pickupCoords.Contains(temp) || grid[temp.x, temp.y].containedObject != null);
            pickupCoords.Add(temp);
        }

        // for each of the previously made coordinates, create a pickup and attach it to that tile.
        for (int i = 0; i < pickupCount; i++)
        {
            GridObject tempPickup = Instantiate(pickupPrefab, transform.position, Quaternion.identity).GetComponent<GridObject>();
            tempPickup.SetCoords(pickupCoords[i]);
            tempPickup.gameObject.name = "Turn Pickup";

            grid[Mathf.FloorToInt(pickupCoords[i].x), Mathf.FloorToInt(pickupCoords[i].y)].SetContainedObject(tempPickup);
        }
    }

    /// <summary>
    /// Creates a new GameObject and places it in world space in accordance with the grid. Adds the tile to the passed grid coordinates, overwriting anything previosuly there.
    /// </summary>
    private void CreateGridTile(int x, int y)
    {
        GridTile tempTile = Instantiate(tilePrefab, transform).GetComponent<GridTile>();
        tempTile.SetCoordinates(x, y);
        tempTile.transform.localScale = new Vector3(tileSize, tileSize, tileSize);

        tempTile.gameObject.name = "GridTile(" + x + "," + y + ")";

        grid[x, y] = tempTile;
    }

    public Vector2 GetWorldCoordinatesFromGrid(int x, int y)
    {
        Vector2 coordinates = new Vector2(x * tileSize + (tileGap * x), y * tileSize + (tileGap * y));

        return coordinates;
    }
    /// <summary>
    /// Returns the world coordinates of the grid's center.
    /// </summary>
    public Vector2 GetGridCenterWorldCoordinate()
    {
        Vector2 max = GetWorldCoordinatesFromGrid(gridWidth, gridHeight);

        return max / 2;
    }

}
