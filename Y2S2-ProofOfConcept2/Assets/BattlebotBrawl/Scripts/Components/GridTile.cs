﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public enum TileRevealState { invisible, fog, reveal };
public class GridTile : MonoBehaviour
{
    // The x and y coordinates of the object on the grid, NOT of the object in world space.
    public int gridX;
    public int gridY;

    public Color fogColor;

    public BattleBot containedBot;
    public List<GridObject> containedObjects;
    public int tileCapacity;

    private GridManager manager;

    public TileRevealState revealState;

    public void Awake()
    {
        containedObjects = new List<GridObject>();
        containedBot = null;
    }
    public void SetContainedBot(BattleBot bot)
    {
        containedBot = bot;
    }
    public BattleBot GetBattleBot()
    {
        return containedBot;
    }
    /// <summary>
    /// Initialize should be called on each new grid tile to set its variables and give it a chance to set itself up.
    /// </summary>
    /// <param name="coordinates">The coordinates of the grid, NOT world coordinates.</param>
    /// <param name="scale">The scale to set the object to.</param>
    /// <param name="m">The GridManager that the tile should communicate with.</param>
    /// <param name="revState">The default reveal state for the tile.</param>
    public void Initialize(Vector2Int coordinates, Vector3 scale, GridManager m, TileRevealState revState = TileRevealState.reveal, int newCapacity = 1)
    {
        SetCoordinates(coordinates.x, coordinates.y);
        transform.localScale = scale;
        gameObject.name = "GridTile(" + gridX + "," + gridY + ")";
        manager = m;

        SetRevealState(revState);
        transform.position = manager.GetWorldCoordinatesFromGrid(gridX, gridY);
        tileCapacity = newCapacity;
    }

    public void ObjectRevealEvent()
    {
        foreach (GridObject i in containedObjects)
        {
            i.CallRevealEvent();
        }
    }
    public void ObjectCollectEvent()
    {
        foreach (GridObject i in containedObjects)
        {
            i.CallCollectEvent();
        }
    }
    public void SetRevealState(TileRevealState state)
    {
        if (revealState != state)
        {
            revealState = state;

            if (state == TileRevealState.invisible)
            {
                SetActiveChildren(false);
            }
            else
            {
                SetActiveChildren(true);
            }

            UpdateRevealState();
        }
    }
    public void UpdateRevealState()
    {
        switch (revealState)
        {
            case TileRevealState.fog:
                GetComponentInChildren<Renderer>().material.color = fogColor;
                break;
            case TileRevealState.reveal:
                GetComponentInChildren<Renderer>().material.color = Color.white;
                break;
        }
    }
    public void SetActiveChildren(bool a)
    {
        foreach (Transform child in transform)
        {
            child.gameObject.SetActive(a);
        }
    }
    /// <summary>
    /// Adds the passed object to the objects contained within the tile. Will shape the object to fit the tile's size by default.
    /// </summary>
    /// <param name="newObj">The object to be placed in the tile</param>
    /// <param name="fitToSize">Wether or not to set the scale, position, and parent of the passed object. True by default.</param>
    /// <param name="forceAdd">Whether or not to forcibly add the object. Will ignore tile's capacity to add it. False by default.</param>
    public void AddContainedObject(GridObject newObj, bool fitToSize = true, bool forceAdd = false)
    {
        if (containedObjects.Count < tileCapacity || forceAdd)
        {
            if (fitToSize)
            {
                newObj.gameObject.transform.parent = transform;
                newObj.transform.position = new Vector3(manager.tileSize / 2, manager.tileSize / 2, -0.35f);
                newObj.transform.localScale = transform.localScale;
            }
            containedObjects.Add(newObj);
        }
    }
    public void SetCoordinates(int x, int y)
    {
        gridX = x;
        gridY = y;
    }
}
