﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public enum GridOrientation { horizontal, vertical };
public class GridManager : MonoBehaviour
{
    public GridOrientation orientation = GridOrientation.vertical;
    public int gridWidth;
    public int gridHeight;
    public float tileSize = 1f;
    public float tileGap = 0f;

    public GameObject tilePrefab;

    public GridTile[,] grid;
    private List<GridObject> gridObjects;

    private void Awake()
    {
        gridObjects = new List<GridObject>();
        GenerateGrid();
    }

    public void RemoveBotFromGrid(BattleBot bot)
    {
        foreach (GridTile i in grid)
        {
            if (i.name == bot.name)
            {
                i.containedBot = null;
            }
        }
    }
    public bool CheckCoords(Vector2Int coords)
    {
        if (coords.x >= 0 && coords.y >= 0 && coords.x < gridWidth && coords.y < gridHeight)
        {
            return true;
        }
        //Debug.LogError("Bot tried to use bad coordinates!");
        return false;
    }
    public void MoveBattleBot(Vector2Int oldPosition, Vector2Int newPosition, BattleBot bot)
    {
        if (CheckCoords(newPosition))
        {
            if (CheckCoords(oldPosition)) grid[oldPosition.x, oldPosition.y].containedBot = null;
            grid[newPosition.x, newPosition.y].SetContainedBot(bot);
        }
    }
    public BattleBot CheckForBattleBots(Vector2Int coords)
    {
        return grid[coords.x, coords.y].GetBattleBot();
    }
    public void RegisterDeath(BreakableObject deadObject)
    {
        gridObjects.Remove(deadObject);
    }
    public void ExploreTile(Vector2Int coordinates)
    {
        ExploreTileSingle(new Vector2Int(coordinates.x, coordinates.y), TileRevealState.reveal, true);
        ExploreTileSingle(new Vector2Int(coordinates.x + 1, coordinates.y), TileRevealState.reveal);
        ExploreTileSingle(new Vector2Int(coordinates.x - 1, coordinates.y), TileRevealState.reveal);
        ExploreTileSingle(new Vector2Int(coordinates.x, coordinates.y + 1), TileRevealState.reveal);
        ExploreTileSingle(new Vector2Int(coordinates.x, coordinates.y - 1), TileRevealState.reveal);
    }
    public void ExploreTileSingle(Vector2Int coords, TileRevealState state, bool callCollectEvent = false)
    {
        if (IsInsideBounds(coords))
        {
            GridTile tile = grid[coords.x, coords.y];
            // If the tile has not been seen yet, call the reveal event.
            if (tile.revealState == TileRevealState.invisible)
                tile.ObjectRevealEvent();
            // True should be passed if the player is on the tile that is being revealed.
            if (callCollectEvent)
                tile.ObjectCollectEvent();
            tile.SetRevealState(state);
        }
    }
    public bool IsInsideBounds(Vector2Int coords)
    {
        return coords.x < gridWidth && coords.x >= 0 && coords.y < gridHeight && coords.y >= 0;
    }

    public void RegisterTile(GridTile newObj)
    {
        grid[newObj.gridX, newObj.gridY] = newObj;
    }

    public void GenerateGrid()
    {
        grid = new GridTile[gridWidth, gridHeight];
        for (int i = 0; i < gridWidth; i++)
        {
            for (int j = 0; j < gridHeight; j++)
            {
                CreateGridTile(i, j);
            }
        }
    }

    /*
    public void GenerateTurnPickups()
    {
        List<Vector2Int> pickupCoords = new List<Vector2Int>();

        // Create each pyramid.
        for (int i = 0; i < pickupCount; i++)
        {
            Vector2Int temp;
            // Ensure there are no duplicate coordinates.
            do
            {
                temp = new Vector2Int(Random.Range(0, gridWidth), Random.Range(0, gridHeight));
            } while (pickupCoords.Contains(temp) || grid[temp.x, temp.y].containedObject != null);
            pickupCoords.Add(temp);
        }

        // for each of the previously made coordinates, create a pickup and attach it to that tile.
        for (int i = 0; i < pickupCount; i++)
        {
            GridObject tempPickup = Instantiate(pickupPrefab, transform.position, Quaternion.identity).GetComponent<GridObject>();
            tempPickup.SetCoords(pickupCoords[i]);
            tempPickup.gameObject.name = "Turn Pickup";

            grid[Mathf.FloorToInt(pickupCoords[i].x), Mathf.FloorToInt(pickupCoords[i].y)].SetContainedObject(tempPickup);
        }
    }
    */

    /// <summary>
    /// Creates a new GameObject and places it in world space in accordance with the grid. Adds the tile to the passed grid coordinates, overwriting anything previosuly there.
    /// </summary>
    public void CreateGridTile(int x, int y)
    {
        GridTile tempTile = Instantiate(tilePrefab, transform).GetComponent<GridTile>();
        tempTile.Initialize(new Vector2Int(x, y), new Vector3(tileSize, tileSize, tileSize), this);

        grid[x, y] = tempTile;
    }

    public Vector3 GetWorldCoordinatesFromGrid(int x, int y)
    {
        //if (CheckCoords(new Vector2Int(x, y)))
        //{
        if (orientation == GridOrientation.vertical)
        {
            return new Vector3(x * tileSize + (tileGap * x), y * tileSize + (tileGap * y));
        }
        else
        {
            return new Vector3(x * tileSize + (tileGap * x), 0, y * tileSize + (tileGap * y));
        }
        //}
        //else
        //{
        //return new Vector3(-1, -1, -1);
        //}
    }
    /// <summary>
    /// Returns the world coordinates of the grid's center.
    /// </summary>
    public Vector2 GetGridCenterWorldCoordinate()
    {
        Vector2 max = GetWorldCoordinatesFromGrid(gridWidth, gridHeight);

        return max / 2;
    }

}
