﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Collapse : MonoBehaviour
{
    public float burstForce = 1f;
    public void CollapseBot()
    {
        foreach(Rigidbody i in GetComponentsInChildren<Rigidbody>())
        {
            i.isKinematic = false;
            i.AddForce(new Vector3(Random.Range(-burstForce, burstForce), Random.Range(-burstForce, burstForce), Random.Range(-burstForce, burstForce)), ForceMode.Impulse);
        }
    }
}
