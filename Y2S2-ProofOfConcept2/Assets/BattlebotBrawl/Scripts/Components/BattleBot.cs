﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BattleBot : BreakableObject
{

    public int damage;

    public int moves = 3;
    protected int movesLeft;
    protected bool turn = true;

    public GridManager gridManager;

    public Color botColor;

    private void Awake()
    {
        NewTurn();
    }
    
    public void ThrowBot(int x, int y, int damage)
    {
        Vector2Int newCoords = new Vector2Int(gridX + x, gridY + y);
        SetWorldPosition(new Vector2Int(gridX, gridY), newCoords);
        gridX = newCoords.x;
        gridY = newCoords.y;
        Damage(damage);
        if (!gridManager.IsInsideBounds(new Vector2Int(gridX, gridY)))
        {
            hp = 0;
            CheckAlive();
        }
    }
    protected override bool CheckAlive()
    {
        if (!base.CheckAlive())
        {
            gridManager.RemoveBotFromGrid(this);
            return false;
        }
        return true;
    }
    public void SetWorldPosition(Vector2Int oldCoords, Vector2Int newCoords)
    {
        Vector3 newPosition = gridManager.GetWorldCoordinatesFromGrid(newCoords.x, newCoords.y);
        gridManager.MoveBattleBot(oldCoords, newCoords, this);
        newPosition.y += 0.5f;
        transform.position = newPosition;
    }
    public void EndTurn()
    {
        movesLeft = 0;
        turn = false;
        SetColor(Color.red);
    }
    public void NewTurn()
    {
        movesLeft = moves;
        turn = true;
        SetColor(botColor);
    }
    public virtual void Move(int x, int y)
    {
        Move(new Vector2Int(x, y));
    }
    public virtual void Move(Vector2Int distance, bool force = false)
    {
        if (turn || force)
        {
            if (gridManager.IsInsideBounds(distance + new Vector2Int(gridX, gridY)))
            {
                Vector2Int newCoords = new Vector2Int(gridX + distance.x, gridY + distance.y);
                SetWorldPosition(new Vector2Int(gridX, gridY), newCoords);
                gridX = newCoords.x;
                gridY = newCoords.y;
                movesLeft--;
                CheckTurnEnd();
            }
        }
    }
    public void SetScale(Vector3 scale)
    {
        transform.GetChild(0).localScale = scale;
    }
    private void CheckTurnEnd()
    {
        if (movesLeft <= 0)
        {
            EndTurn();
        }
    }
    public void SetColor(Color col)
    {
        foreach (Renderer i in GetComponentsInChildren<Renderer>())
        {
            i.material.color = col;
        }
    }
}
