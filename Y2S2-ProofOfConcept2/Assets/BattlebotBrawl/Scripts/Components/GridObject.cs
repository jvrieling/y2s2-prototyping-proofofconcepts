﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GridObject : MonoBehaviour
{
    // The x and y coordinates of the object on the grid, NOT of the object in world space.
    public int gridX;
    public int gridY;

    public void CallRevealEvent()
    {

    }
    public void CallCollectEvent()
    {

    }

    public void SetCoords(Vector2 newCoords)
    {
        gridX = Mathf.FloorToInt(newCoords.x);
        gridY = Mathf.FloorToInt(newCoords.y);
    }
}
