﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BreakableObject : GridObject
{
    public int hp = 3;

    protected bool alive = true;

    public string onDeathSendMessage = "";

    public void Damage(int amt)
    {
        hp -= amt;
        CheckAlive();
    }
    protected virtual bool CheckAlive()
    {
        if(hp <= 0 && alive)
        {
            alive = false;

            if(onDeathSendMessage != "")
            {
                gameObject.SendMessage(onDeathSendMessage);
            }
            return false;
        }
        return true;
    }
}
