﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IdleController : MonoBehaviour
{
    private void Start()
    {
        GetComponent<BattleBot>().SetWorldPosition(new Vector2Int(-1, -1), new Vector2Int(GetComponent<BattleBot>().gridX, GetComponent<BattleBot>().gridY));
    }
}
