﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class PlayerController : BattleBot
{
    public GameObject heldBattleBot;
    public bool holdingBot = false;
    public int throwRange = 3;

    private void Start()
    {
        SetWorldPosition(new Vector2Int(-1, -1), new Vector2Int(gridX, gridY));
    }
    void Update()
    {
        if(Input.mouseScrollDelta.y != 0)
        {
            throwRange = Mathf.Clamp(throwRange + (int)Input.mouseScrollDelta.y, 0, 8);
        }
        if (alive)
        {
            if (heldBattleBot == null)
            {
                if (movesLeft > 0)
                {
                    if (Input.GetKeyDown(KeyCode.UpArrow) || Input.GetKeyDown(KeyCode.W))
                    {
                        Move(0, 1);
                    }
                    if (Input.GetKeyDown(KeyCode.DownArrow) || Input.GetKeyDown(KeyCode.S))
                    {
                        Move(0, -1);
                    }
                    if (Input.GetKeyDown(KeyCode.LeftArrow) || Input.GetKeyDown(KeyCode.A))
                    {
                        Move(-1, 0);
                    }
                    if (Input.GetKeyDown(KeyCode.RightArrow) || Input.GetKeyDown(KeyCode.D))
                    {
                        Move(1, 0);
                    }
                }
            }
            else
            {
                if (Input.GetKeyDown(KeyCode.UpArrow) || Input.GetKeyDown(KeyCode.W))
                {
                    ThrowHeldBot(0, throwRange);
                }
                if (Input.GetKeyDown(KeyCode.DownArrow) || Input.GetKeyDown(KeyCode.S))
                {
                    ThrowHeldBot(0, -throwRange);
                }
                if (Input.GetKeyDown(KeyCode.LeftArrow) || Input.GetKeyDown(KeyCode.A))
                {
                    ThrowHeldBot(-throwRange, 0);
                }
                if (Input.GetKeyDown(KeyCode.RightArrow) || Input.GetKeyDown(KeyCode.D))
                {
                    ThrowHeldBot(throwRange, 0);
                }
            }
        }
    }
    public void ThrowHeldBot(int x, int y)
    {
        BattleBot held = heldBattleBot.GetComponent<BattleBot>();
        held.ThrowBot(x, y, 1);
        heldBattleBot = null;
    }
    public override void Move(int x, int y)
    {
        BattleBot otherBot = gridManager.CheckForBattleBots(new Vector2Int(gridX + x, gridY + y));
        if (otherBot == null)
        {
            base.Move(x, y);
        }
        else
        {
            heldBattleBot = otherBot.gameObject;
        }
    }
}
