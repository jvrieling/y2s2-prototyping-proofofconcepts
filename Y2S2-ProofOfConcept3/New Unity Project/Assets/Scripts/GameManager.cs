﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameManager : MonoBehaviour
{
    public static GameManager instance;
    public SpriteRenderer potionLiquid;
    public GameObject baseSet;
    public GameObject ingredientSet;
    public GameObject catalystSet;
    public GameObject resultSet;
    public Text resultsText;

    public int set = 0;

    public int liquid = 0;
    public int ingredient = 0;
    public int catalyst = 0;

    public Color baseColor;

    private void Awake()
    {
        instance = this;
    }
    private void Start()
    {
        resultSet.SetActive(false);
        catalystSet.SetActive(false);
        baseSet.SetActive(true);
        ingredientSet.SetActive(false);
    }

    public void NextButtonSet()
    {
        if(set == 0)
        {
            baseSet.SetActive(false);
            ingredientSet.SetActive(true);
            set = 1;
        } else if (set == 1)
        {
            ingredientSet.SetActive(false);
            catalystSet.SetActive(true);
            set = 2;
        } else if(set == 2)
        {
            resultSet.SetActive(true);
            catalystSet.SetActive(false);

            resultsText.text = "Potion Type: " + GetPotionType() + 
                "\n Potion Effect: " + GetPotionEffect() +
                "\n Potion Strength: " + GetPotionStrength();

        }
    }
    public string GetPotionStrength()
    {
        return "" + liquid + ingredient;
    }
    public string GetPotionEffect()
    {
        switch ("" + liquid + ingredient)
        {
            case "11":
                return "Sparkling";
            case "21":
                return "Healing";
            case "31":
                return "Strength";
            case "12":
                return "Levitation";
            case "33":
                return "Poison";
        }

        return "Gobbly Goop";
    }
    public string GetPotionType()
    {
        switch (catalyst)
        {
            case 1:
                return "Throwable Splash";
            case 2:
                return "Throwable Lingering";
            case 3:
                return "Drinkable";
        }

        return "Basic";
    }
    public void SetBase(string liq)
    {
        switch (liq)
        {
            case "water":
                liquid = 1;
                break;
            case "tears":
                liquid = 2;
                break;
            case "acid":
                liquid = 3;
                break;
        }

        UpdatePotionColor();
    }
    public void SetIngredient(string liq)
    {
        switch (liq)
        {
            case "stardust":
                ingredient = 1;
                break;
            case "fang":
                ingredient = 2;
                break;
            case "frog":
                ingredient = 3;
                break;
        }

        UpdatePotionColor();
    }
    public void SetCatalyst(string cat)
    {
        switch (cat)
        {
            case "gunpowder":
                catalyst = 1;
                break;
            case "dragontear":
                catalyst = 2;
                break;
            case "pineapple":
                catalyst = 3;
                break;
        }

        UpdatePotionColor();
    }

    public void UpdatePotionColor()
    {
        if (liquid == 1)
        {
            baseColor = new Color32(100, 200, 255, 255);
        }
        else if (liquid == 2)
        {
            baseColor = new Color32(140, 255, 255, 255);
        }
        else if (liquid == 3)
        {
            baseColor = new Color32(11, 214, 41, 255);
        }

        Color32 updatedColor = baseColor - new Color32(50, 50, 50, 0);

        switch (ingredient)
        {
            case 1:
                updatedColor.g -= 30;
                updatedColor.r -= 30;
                updatedColor.b -= 30;
                break;
            case 2:
                updatedColor.g -= 60;
                updatedColor.r += 50;
                updatedColor.b += 50;
                break;
            case 3:
                updatedColor.g += 50;
                updatedColor.r += 50;
                updatedColor.b -= 60;
                break;
        }

        switch (catalyst)
        {
            case 1:
                updatedColor.g += 50;
                updatedColor.r += 50;
                updatedColor.b += 20;
                break;
            case 2:
                updatedColor.g -= 50;
                updatedColor.r += 50;
                updatedColor.b -= 50;
                break;
            case 3:
                updatedColor.g += 50;
                updatedColor.r -= 50;
                updatedColor.b -= 50;
                break;
        }

        potionLiquid.color = updatedColor;
    }
}
