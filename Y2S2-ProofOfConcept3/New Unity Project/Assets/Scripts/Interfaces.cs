﻿public interface IDrinkable
{
    void Drink();
}

public interface ISplashable<T>
{
    void Throw(T target);
}

public interface IAffectable<T>
{
    void AffectTarget(T target);
}
